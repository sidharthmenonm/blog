---
layout: post
title: How to split last word in google sheets
author: sidharth
categories:
- Google Sheets
- Excel
- Regex
- Regextract
tags:
- google
- sheets
- tricks
image: assets/images/Screenshot%202020-09-27%20at%2012.43.24%20PM.png
description: This post shows you how to split the last word out of a phrase in google
  sheets.
featured: true
hidden: false
comments: false
rating: 4.5
---

Over the course of time while using google sheets you might have came across a situation where you want to split the last word from a column and there is no usable seperator to reference.

![](/assets/images/Screenshot%202020-09-27%20at%2012.43.24%20PM.png)

Obviously as always when we do a google search we can find that there is a formula to extract the last word from the column.

```
=TRIM(RIGHT(SUBSTITUTE(A1," ",REPT(" ",255)),255))
```

And this works perfectly when you want to extract the last word.

![](/assets/images/Screenshot%202020-09-27%20at%2012.49.46%20PM.png)

As you can see this extracts the last word but it dosent remove the same from the remaining part. What if you want the remaining portion also seperated.

![](/assets/images/Screenshot%202020-09-27%20at%201.10.08%20PM.png)

### Enter REGEX

REGEXTRACT allows you to use regular expressions (regex) to extract. You can read more on regex [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions). 

In google sheets three regex formulas are available, each serving a different purpose as the name indicates.

```
=REGEXMATCH
=REGEXEXTRACT
=REGEXREPLACE
```

There are multiple ways to acheive the result we want.

## 1. REGEXEXTRACT

This forumale basically extracts the portion of the text matched by the regular expression given.

```
=REGEXEXTRACT(text, regular_expression)
```

The regular expression that we are going to use is `^([\w\s]*)\s` 

> You can learn build and test regex by visiting the [https://regexr.com/](https://regexr.com/)

In google sheet the `text` portion is to be selected as the cell that contains the original text. in our case the formaule looks like this

```
=REGEXEXTRACT(A2,"^([\w\s]*)\s")
```

![](/assets/images/Screenshot%202020-09-27%20at%201.47.00%20PM.png)

## 2. REGEXREPLACE
This is a much simpler way which involved replacing the already known portion from the original value.

```
=REGEXREPLACE(text, regular_expression, replacement)
```

In our case we are going to use the following formula

```
=REGEXREPLACE(A2,C2,"")
```

Here `A2` has the original text and `C2` contains the already extracted part that needs to be replaced and we replace with an empty string.

![](/assets/images/Screenshot%202020-09-27%20at%201.54.21%20PM.png)
